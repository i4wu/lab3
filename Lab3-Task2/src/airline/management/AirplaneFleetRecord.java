package airline.management;

public class AirplaneFleetRecord {

	private Airplane airplane;
	private int numberOfPlanes;

	public AirplaneFleetRecord(Airplane airplane) {
		this.airplane = airplane;
	}

	public int getNumberOfPlanes() {
		return numberOfPlanes;
	}

	public void setNumberOfPlanes(int numberOfPlanes) {
		this.numberOfPlanes = numberOfPlanes;
	}

}
