package airline.management;

import java.util.HashMap;
import java.util.Map;


public class Airline {
	private String name;
	private Map<Airplane,AirplaneFleetRecord> fleet;
	
	public Airline(String name) {
		this.name = name;
		this.fleet = new HashMap<Airplane,AirplaneFleetRecord>();
	}

	public int getNumberOfPlanes(Airplane airplane){
		if (this.fleet.get(airplane) == null) {
			return 0;
		}
		else {
			return this.fleet.get(airplane).getNumberOfPlanes();
		}
	}
	
	public void addAirplaneFleetRecord(Airplane a) throws AirlineException{
		if (this.getAirplaneFleetRecord(a) == null) {
			AirplaneFleetRecord record = new AirplaneFleetRecord(a);
			this.fleet.put(a, record);
		}
		else {
			throw new AirlineException();
		}
	}
	public boolean isInTheFleet(Airplane a){
		if (this.fleet.get(a) != null) {
			return true;
		}
		else {
			return false;
		}
	}
	public AirplaneFleetRecord getAirplaneFleetRecord(Airplane a){
		return this.fleet.get(a);
	}
	
	public void buyAirplane(Airplane a, int quantity) throws AirlineException {
		if (this.fleet.get(a) == null) {
			throw new AirlineException();
		}
		else {
			this.fleet.get(a).setNumberOfPlanes(quantity + this.fleet.get(a).getNumberOfPlanes());
		}
	}
	public void sellAirplane(Airplane a, int quantity) throws AirlineException {
		if (this.fleet.get(a) == null) {
			throw new AirlineException();
		}
		else if (this.fleet.get(a).getNumberOfPlanes() < quantity) {
			throw new AirlineException();
		}
		else {
			this.fleet.get(a).setNumberOfPlanes(this.fleet.get(a).getNumberOfPlanes() - quantity);
		}
	}

}