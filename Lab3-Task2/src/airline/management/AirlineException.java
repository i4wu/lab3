package airline.management;

public class AirlineException extends Exception {

	private static final long serialVersionUID = 1L;

	public AirlineException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AirlineException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public AirlineException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public AirlineException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	

}
