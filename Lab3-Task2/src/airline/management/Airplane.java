package airline.management;




public class Airplane {
	private String model;
	private String manufacturer;
	private String typeOfUsage;
	
	public Airplane(String model, String manufacturer, String typeOfUsage) throws AirlineException {
		if(typeOfUsage != "civilian" && 
			typeOfUsage != "commercial" &&
			typeOfUsage != "privatejet") {
			throw new AirlineException();
		}
		
		this.model = model;
		this.manufacturer = manufacturer;
		this.typeOfUsage = typeOfUsage;
	}

	public String getModel() {
		return model;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getTypeOfUsage(){
		return typeOfUsage;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((manufacturer == null) ? 0 : manufacturer.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airplane other = (Airplane) obj;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		return true;
	} 
	
	
}
