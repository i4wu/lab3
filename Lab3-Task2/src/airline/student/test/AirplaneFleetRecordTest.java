package airline.student.test;

import static org.junit.Assert.*;

import org.junit.Test;

import airline.management.AirlineException;
import airline.management.Airplane;
import airline.management.AirplaneFleetRecord;

public class AirplaneFleetRecordTest {

	@Test
	public void testAirplaneFleetRecord() throws AirlineException {
		Airplane testPlane = new Airplane("Paper", "Boeing", "civilian");
		AirplaneFleetRecord testRecord = new AirplaneFleetRecord(testPlane);
		
		assertEquals(testRecord.getNumberOfPlanes(), 0);
	}

	@Test
	public void testGetNumberOfPlanes() throws AirlineException {
		Airplane testPlane = new Airplane("Paper", "Boeing", "civilian");
		AirplaneFleetRecord testRecord = new AirplaneFleetRecord(testPlane);

		assertEquals(testRecord.getNumberOfPlanes(), 0);
		
		for (int i = 0; i < 100; i++) {
			testRecord.setNumberOfPlanes(i);
			assertEquals(testRecord.getNumberOfPlanes(), i);
		}
	}

	@Test
	public void testSetNumberOfPlanes() throws AirlineException {
		Airplane testPlane = new Airplane("Paper", "Boeing", "civilian");
		AirplaneFleetRecord testRecord = new AirplaneFleetRecord(testPlane);
		
		for (int i = 0; i < 100; i++) {
			testRecord.setNumberOfPlanes(i);
			assertEquals(testRecord.getNumberOfPlanes(), i);
		}
	}

}
