package airline.student.test;

import static org.junit.Assert.*;

import org.junit.Test;

import airline.management.AirlineException;
import airline.management.Airplane;

public class AirplaneTest {
	@Test
	public void testHashCode() throws AirlineException {
		Airplane testPlane1 = new Airplane("uno","dos","civilian");
		Airplane testPlane2 = new Airplane("uno","dos","civilian");
		
		assertTrue(testPlane1.hashCode()==testPlane2.hashCode());
	}

	@Test
	public void testAirplane() {
		try {
			Airplane testPlane1 = new Airplane("uno","dos","tres");
			fail("invalid usage");
		}
		catch(AirlineException e) {
			//do nothing
		}
	}

	@Test
	public void testGetModel() throws AirlineException {
		Airplane testPlane1 = new Airplane("uno","dos","civilian");
		Airplane testPlane2 = new Airplane("uno","dos","civilian");
		Airplane testPlane3 = new Airplane("onu","dos","civilian");
		
		assertTrue(testPlane1.getModel().equals(testPlane2.getModel()));
		assertFalse(testPlane1.getModel().equals(testPlane3.getModel()));
	}

	@Test
	public void testGetManufacturer() throws AirlineException {
		Airplane testPlane1 = new Airplane("uno","dos","civilian");
		Airplane testPlane2 = new Airplane("uno","dos","civilian");
		Airplane testPlane3 = new Airplane("uno","sod","civilian");
		
		assertTrue(testPlane1.getManufacturer().equals(testPlane2.getManufacturer()));
		assertFalse(testPlane1.getManufacturer().equals(testPlane3.getManufacturer()));
	}

	@Test
	public void testGetTypeOfUsage() throws AirlineException {
		Airplane testPlane1 = new Airplane("uno","dos","civilian");
		Airplane testPlane2 = new Airplane("uno","dos","civilian");
		Airplane testPlane3 = new Airplane("uno","dos","commercial");
		
		assertTrue(testPlane1.getTypeOfUsage().equals(testPlane2.getTypeOfUsage()));
		assertFalse(testPlane1.getTypeOfUsage().equals(testPlane3.getTypeOfUsage()));
	}

	@Test
	public void testEqualsObject() throws AirlineException {
		Airplane testPlane1 = new Airplane("uno","dos","civilian");
		Airplane testPlane2 = new Airplane("uno","dos","civilian");
		
		Airplane testPlane3 = new Airplane("uno","sod","civilian");
		Airplane testPlane4 = new Airplane("onu","dos","civilian");
		Airplane testPlane5 = new Airplane("onu","sod","civilian");
		
		assertTrue(testPlane1.equals(testPlane2));
		
		assertFalse(testPlane1.equals(testPlane3));
		assertFalse(testPlane1.equals(testPlane4));
		assertFalse(testPlane1.equals(testPlane5));
	}

}
