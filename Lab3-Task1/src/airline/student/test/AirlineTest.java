package airline.student.test;

import static org.junit.Assert.*;

import org.junit.Test;

import airline.management.Airline;
import airline.management.AirlineException;
import airline.management.Airplane;
import airline.management.AirplaneFleetRecord;

public class AirlineTest {
	@Test
	public void testGetNumberOfPlanes() throws AirlineException {
		Airline testLine = new Airline("test");
		Airplane testPlane = new Airplane("test","test","civilian");
		assertTrue(testLine.getNumberOfPlanes(testPlane) == 0);
	}

	@Test
	public void testAddAirplaneFleetRecord() throws AirlineException {
		Airplane testPlane = new Airplane("Paper","Boeing","civilian");
		Airline testAirline = new Airline("TestLine");
		
		testAirline.addAirplaneFleetRecord(testPlane);
		
		assertTrue(testAirline.isInTheFleet(testPlane));
		
		try {
			testAirline.addAirplaneFleetRecord(testPlane);
			fail("Exception should be thrown");
		} catch(AirlineException exception) {
			
		}
	}

	@Test
	public void testIsInTheFleet() throws AirlineException {
		Airline testAirline = new Airline("TestLine");
		Airplane testPlane = new Airplane("Paper", "Boeing", "civilian");
		
		assertFalse(testAirline.isInTheFleet(testPlane));
		
		testAirline.addAirplaneFleetRecord(testPlane);
		
		assertTrue(testAirline.isInTheFleet(testPlane));
	}

	@Test
	public void testGetAirplaneFleetRecord() throws AirlineException {
		Airline testAirline = new Airline("TestLine");
		Airplane testPlane = new Airplane("Paper", "Boeing", "civilian");
		
		assertEquals(testAirline.getAirplaneFleetRecord(testPlane), null);
		
		testAirline.addAirplaneFleetRecord(testPlane);
		
		assertTrue(testAirline.getAirplaneFleetRecord(testPlane).getNumberOfPlanes() == 0);
		
		testAirline.buyAirplane(testPlane, 3);
		
		assertTrue(testAirline.getAirplaneFleetRecord(testPlane).getNumberOfPlanes() == 3);
	}

}
